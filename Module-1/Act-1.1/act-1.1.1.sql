SELECT * FROM db1.tetu;

#question1

use db1;
CREATE TABLE tetu (
pk_numsecu CHAR(13) PRIMARY KEY,
k_numetu VARCHAR (20) UNIQUE NOT NULL,
nom VARCHAR(50),
prenom VARCHAR(50));
desc tetu;

#question2

INSERT INTO tetu (pk_numsecu, k_numetu, nom, prenom)
VALUES ('2820475001124', 'XGB67668', 'Durand', 'Anne');
INSERT INTO tetu (pk_numsecu, k_numetu, nom, prenom)
VALUES ('1800675001066', 'AB3937098X', 'Dupont', 'Pierre');

#question3
SELECT pk_numsecu, k_numetu,nom, prenom
FROM tetu;

#question4

SELECT nom, prenom
FROM tetu
WHERE pk_numsecu='2820475001124';