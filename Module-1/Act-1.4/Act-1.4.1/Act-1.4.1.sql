USE LABORATOIRE;
CREATE TABLE medicament (
nom VARCHAR(100) PRIMARY KEY,
descriptionC VARCHAR(100),
descriptionL VARCHAR(500),
nbrepill integer);

desc medicament;

INSERT INTO medicament (nom, descriptionC, descriptionL, nbrepill)
VALUES ('Chorix', 'Médicament contre la chute des choux', 'Vivamus fermentum semper porta.Nunc diam velit,adipiscing ut tristique vitae,sagittis vel odio. Maecenas convallis ullamcorper ultricies. Curabitur ornare.', 13);
INSERT INTO medicament (nom, descriptionC, descriptionL, nbrepill)
VALUES ('Tropas', 'Médicament contre les dysfonctionnements intellectuels', 'Suspendisse lectus leo, consectetur in tempor sit amet, placerat quis neque. Etiam luctus porttitor lorem, sed suscipit est rutrum non', 42);

select*
from medicament;

USE LABORATOIRE;
CREATE TABLE contreindication (
cod VARCHAR(100) PRIMARY KEY,
descrip VARCHAR(100),
medicament VARCHAR(100),
foreign key(medicament) references medicament(nom));

desc medicament;

INSERT INTO contreindication (cod, descrip, medicament)
VALUES ('CI1', 'ne jamais prendre après minuit', 'Chorix');

INSERT INTO contreindication (cod, descrip, medicament)
VALUES ('CI2', 'ne jamais mettre en contact avec de l eau', 'Chorix');

INSERT INTO contreindication (cod, descrip, medicament)
VALUES ('CI3', 'garder à l abri de la lumière du soleil', 'Tropas');

select*
from contreindication


