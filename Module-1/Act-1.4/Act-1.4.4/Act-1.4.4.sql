USE LABORATOIRE;
CREATE TABLE spectacle(
nospectacles INTEGER PRIMARY KEY,
nom VARCHAR(100),
durée INTEGER
CHECK (durée between 0 and 90),
typ VARCHAR(50)
CHECK (typ in ('theatre','dance','concert')));

CREATE TABLE representation(
dat date,
prix INTEGER,
CHECK (prix <=25));

INSERT INTO  representation (dat, prix )
VALUES ('2022-01-11' ,30);






desc spectacle;

DROP TABLE spectacle;
DROP TABLE representation;
INSERT INTO spectacle (nospectacles, nom, durée, typ)
VALUES ('null ', 'famille', '70', 'theatre');

select* 
from representation;