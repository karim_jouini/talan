# question2
drop table produit;
drop table consommateur;
drop table producteur;
USE LABORATOIRE;
CREATE TABLE consommateur(
login VARCHAR(20),
email VARCHAR(50),
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
ville VARCHAR(255) NOT NULL,
PRIMARY KEY(login,email),
UNIQUE(nom,prenom,ville));



CREATE TABLE producteur(
raisonsoc VARCHAR(25) PRIMARY KEY,
ville VARCHAR(50));



CREATE TABLE produit(
id INTEGER PRIMARY KEY,
descrip VARCHAR(100),
produitpar VARCHAR(100),
consommeparlog VARCHAR(100),
consommeparemail VARCHAR(100),
foreign key(produitpar) references producteur(raisonsoc),
foreign key(consommeparlog,consommeparemail) references consommateur(login,EMAIL));

select* from produit;
