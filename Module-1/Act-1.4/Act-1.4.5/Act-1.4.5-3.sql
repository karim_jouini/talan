-- question3

INSERT INTO consommateur (login, email, nom, prenom, ville)
VALUES ('Al', 'Al.Un@compiegne.fr', 'Un', 'Al', 'Compiègne'),
('Bob', 'Bob.Deux@compiegne.fr', 'Deux', 'Bob', 'Compiègne'),
('Charlie', 'Charlie.Trois@compiegne.fr', 'Charlie', 'Trois', 'Compiègne');

INSERT INTO producteur (raisonsoc, ville)
VALUES ('Pomme Piardes SARL', 'Compiègne');

INSERT INTO produit (id, descrip, produitpar)
VALUES (1, 'Pommes', 'Pomme Piardes SARL'),
(2, 'Pommes', 'Pomme Piardes SARL'),
(3, 'Pommes', 'Pomme Piardes SARL'),
(4, 'Pommes', 'Pomme Piardes SARL'),
(5, 'Cidre', 'Pomme Piardes SARL'),
(6, 'Cidre', 'Pomme Piardes SARL');


select * from produit