Activité 1.5.1 : Employés et départements

Question 1 :
•	Restriction
R = RESTRICTION (EMP, SAL+COMM>10000)
•	Projection
R’ = PROJECTION ( R, R.ENOM)
Question 2 :
•	Projection
R = RESTRICTION (EMP, ENO=10)
•	Restriction
R’ = PROJECTION (R, R.ENOM, R.PROF) 
Question 3 :
•	Jointure
R = JOINTURE (EMP, DEPT, DNO.DEPT=DIR.DMP)
•	Restriction
R’ = RESTRICTION (R, VILLE=’Paris’)
•	Projection
R’’ = PROJECTION (R’, R’.ENOM) 
Question 4 :
•	Jointure
R = JOINTURE (EMP, DEPT, DNO.DEPT=DIR.DMP)
•	Restriction
R’ = RESTRICTION (R, DNOM=’COMMECIAL’)
•	Projection
R’’ = PROJECTION (R’, R’.ENOM) 
Question 5 :
•	Jointure
R = JOINTURE (EMP, DEPT, DNO.DEPT=DIR.DMP)
•	Projection
R’ = PROJECTION (R’, R’.PROF) 
Question 6 :
•	Jointure
R = JOINTURE (EMP, DEPT, DNO.DEPT=DIR.DMP)
•	Restriction
R’ = RESTRICTION (R, PROF=’Ingénieur’)
•	Projection
R’’ = PROJECTION (R’, R’.NOM) 
