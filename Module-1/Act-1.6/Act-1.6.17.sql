DROP TABLE IF EXISTS historique_membre;

CREATE TABLE historique_membre (
  id_membre int(11) NOT NULL,
  id_film int(11) NOT NULL,
  date datetime NOT NULL,
  KEY id_membre (id_membre),
  KEY id_film (id_film)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table historique_membre
--

LOCK TABLES historique_membre WRITE;

INSERT INTO historique_membre VALUES (14,378,'1999-03-01 00:00:00'),(14,395,'1999-03-12 00:00:00'),(14,381,'1999-03-03 00:00:00'),(14,380,'1999-02-24 00:00:00'),(14,397,'1999-05-03 00:00:00'),(14,382,'1999-02-06 00:00:00'),(14,387,'1999-02-16 00:00:00'),(18,391,'1999-04-06 00:00:00'),(18,343,'1998-12-24 00:00:00'),(18,375,'1999-02-19 00:00:00'),(18,376,'1999-02-07 00:00:00'),(18,374,'1999-01-28 00:00:00'),(20,290,'1998-09-22 00:00:00'),(20,389,'1999-03-17 00:00:00');

UNLOCK TABLES;

     
SELECT count(id_film) 
FROM historique_membre
where historique_membre.date BETWEEN '2006-10-30' AND '2007-02-27';
SELECT count(id_film) as nbr_film_noel ,
year(historique_membre.date)as year from historique_membre 
where month(historique_membre.date)=12 and day(historique_membre.date)=24 group by year(historique_membre.date);
