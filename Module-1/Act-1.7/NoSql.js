use dbkarim;

db.createCollection("contact");
db.contact.insertOne
(
{
    "nom": "Saliha",
    "dep":"info"
}
);


db.contact.insertOne
(
{
    "nom": "Poitras",
    "dep":
    {"code":420, nom:"info"},
    "cours":"kba"
}
);


db.programme.insertOne(
{"_id":21, "numad": 11, "nom":"Ruby" , "prenom":"Robin"});

db.programmes.insertMany([{"_id":1, "numad": 11000, "nom":"Patoche" , "prenom":"Alain"},
{"_id":2, "numad": 1200, "nom":"Patoche" , "prenom":"Voila"},
{"_id":3, "numad": 1300, "nom":"Lechat" , "prenom":"Simba"}
]);


db.programmes.insertMany([
{ "_id":4 ,"numad": "2000", "nom":"Gable" ,"prenom":"Alain",
"programme": {"code":420,"nomprog":"info"}
},
{ "_id":5 ,"numad": "2000", "nom":"Leroy" ,"prenom": "Yanick",
"programme": {"code":410,"nomprog":"soins"}
},
]);


db.programmes.find({"nom":"Patoche"}); 
db.programmes.find({"nom":"Patoche", "prenom":"Alain"}); 
db.programme.find({"nom":"Patoche"},{"nom":1,"prenom":1}); 
db.programmes.find({"programme":{"code":420,"nomprog":"info"}});



db.createCollection("employes");
db.employes.insertMany([{"_id":1,"nom":"Patoche" , "Salaire":50000},
{"_id":2, "nom":"Patoche" , "Salaire":45000},
{"_id":3, "nom":"Lechat" , "Salaire":35000}
]);
db.employes.find({"Salaire": {$gt:45000}});
db.employes.find( {"Salaire": {$in:[45000,50000,35000]}});

db.employes.update (
{"_id":1},
{
$inc: {"Salaire": 500}
}
);

db.programmes.remove({"_id":1});
db.programmes.remove({"nom":"Lechat"});

db.programmes.count();
db.programmes.count({"nom":"Patoche"});




















