#Q1
list=[17, 38, 10, 25, 72]
number_of_elements = len(list)
print("Number of elements in the list: ", number_of_elements)

#Q2
list.append(12)
print('Updated list:', list)

#Q3
list.reverse()
print('reversed_list:', list)

#Q4
index=list.index(17)
print(index)

#Q5
print (list[3])

#Q6
list.remove(38)
print(list)

#Q7
print(list[-1])

#Q8
index=list.index(10)
print(index)

#Q9
print(list[-3:])
