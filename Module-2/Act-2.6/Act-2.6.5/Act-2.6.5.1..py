import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="jouini-07454280",
  database="mydatabase"
)
mycursor = mydb.cursor()
mycursor.execute("CREATE TABLE FILMS (ENO INT AUTO_INCREMENT PRIMARY KEY, ENOM VARCHAR(255), PROF VARCHAR(255), SAL FLOAT, COMM FLOAT, DNO INT)")

sql = "INSERT INTO EM (ENOM, PROF, SAL, COMM, DNO) VALUES (%s, %s, %s, %s, %s)"
val = [
  ('Carin', 'directeur', 19000, 120, 5),
  ('Jhon', 'médecin', 30000, 190, 3),
  ('Michelle', 'dentiste', 45000, 250, 3),
  ('Sammuel', 'plombier', 8000, 200, 5),
  ('Stéphane', 'ingénieur', 42000, 400, 4),
  ('François', 'pharmacien', 12000, 170, 3),
  ('Miriam', 'ingénieur', 23000, 270, 1),
  ('Paul', 'pharmacien', 41000, 70, 3),
  ('Nicola', 'technicien', 24000, 380, 5),
  ('Elizabeth', 'développeur', 37000, 310, 5),
  ('Leila', 'technicien', 29000, 230, 2)
]
mycursor.executemany(sql, val)
mydb.commit()
print(mycursor.rowcount, "was inserted.")
