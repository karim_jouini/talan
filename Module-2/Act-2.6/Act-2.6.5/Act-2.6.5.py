import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="jouini-07454280",
  database="mydatabase"
)

mycursor = mydb.cursor()
mycursor.execute("CREATE TABLE EM (ENO INT AUTO_INCREMENT PRIMARY KEY, ENOM VARCHAR(255), PROF VARCHAR(255), SAL FLOAT, COMM FLOAT, DNO INT)")

sql = "INSERT INTO EM (ENOM, PROF, SAL, COMM, DNO) VALUES (%s, %s, %s, %s, %s)"
val = [
  ('Carin', 'directeur', 19000, 120, 5),
  ('Jhon', 'médecin', 30000, 190, 3),
  ('Michelle', 'dentiste', 45000, 250, 3),
  ('Sammuel', 'plombier', 8000, 200, 5),
  ('Stéphane', 'ingénieur', 42000, 400, 4),
  ('François', 'pharmacien', 12000, 170, 3),
  ('Miriam', 'ingénieur', 23000, 270, 1),
  ('Paul', 'pharmacien', 41000, 70, 3),
  ('Nicola', 'technicien', 24000, 380, 5),
  ('Elizabeth', 'développeur', 37000, 310, 5),
  ('Leila', 'technicien', 29000, 230, 2)
]
mycursor.executemany(sql, val)
mydb.commit()
print(mycursor.rowcount, "was inserted.")

mycursor = mydb.cursor()
mycursor.execute("CREATE TABLE DE (DNO INT AUTO_INCREMENT PRIMARY KEY, DNOM VARCHAR(255),DIR INT, VILLE VARCHAR(255))")
sql = "INSERT INTO DE (DNOM, DIR, VILLE) VALUES (%s, %s, %s)"
val = [
  ('Commercial', 3, 'Toulouse'),
  ('Marketing', 9, 'Paris'),
  ('Géstion', 1, 'Nice'),
  ('Sécurité', 5, 'Bordeaux'),
  ('Environnement', 4, 'Nice')
]
mycursor.executemany(sql, val)
mydb.commit()
print(mycursor.rowcount, "was inserted.")

mycursor = mydb.cursor()
sql = "DROP TABLE EM"
mycursor.execute(sql)

mycursor.execute('ALTER TABLE EM ADD foreign key(DNO) references DE(DNO)')
mycursor.execute('ALTER TABLE DE ADD foreign key(DIR) references EM(ENO)')

#Question 1:

mycursor = mydb.cursor()

sql = "SELECT * FROM EM WHERE SAL > 10000"

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)

#Question 2:

mycursor = mydb.cursor()

sql = "SELECT * FROM EM WHERE ENO = 10"

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)

#Question 3:
mycursor = mydb.cursor()

sql = "SELECT EM.ENOM FROM EM INNER JOIN DE ON EM.DNO = DE.DNO WHERE VILLE ='Paris'"
mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)


#Question 4:
mycursor = mydb.cursor()

sql = "SELECT EM.ENOM FROM EM INNER JOIN DE ON DE.DIR = EM.DNO WHERE DNOM ='COMMERCIAL'"
mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)

#Question 5:
mycursor = mydb.cursor()

sql = "SELECT EM.ENOM FROM EM INNER JOIN DE ON DE.DIR = EM.DNO"
mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)

#Question 6:
mycursor = mydb.cursor()

sql = "SELECT  EMP.ENOM  FROM EMP INNER JOIN DEPT ON EMP.ENO = DEPT.DIR where PROF='ingenieur' "
mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)
