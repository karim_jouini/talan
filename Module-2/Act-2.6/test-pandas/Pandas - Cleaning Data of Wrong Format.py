#Convert Into a Correct Format
import pandas as pd
df = pd.read_csv("https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
df.dropna(subset=['Date'], inplace = True)
print(df.to_string())

#Removing Rows

df['Date'] = pd.to_datetime(df['Date'])
print(df.to_string())