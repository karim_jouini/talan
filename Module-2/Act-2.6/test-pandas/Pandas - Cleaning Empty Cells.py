#Empty Cells
import pandas as pd
df = pd.read_csv('https://www.w3schools.com/python/pandas/dirtydata.csv.txt')
new_df = df.dropna()
print(new_df.to_string())

#Remove all rows with NULL values
df = pd.read_csv("https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
df.dropna(inplace = True)
print(df.to_string())

#Replace Empty Values

df.fillna(130, inplace = True)

#Replace Only For Specified Columns

df["Calories"].fillna(130, inplace = True)

#Replace Using Mean
x = df["Calories"].mean()
df["Calories"].fillna(x, inplace = True)
print(df.to_string())

#Replace Using Mredian
x = df["Calories"].median()
df["Calories"].fillna(x, inplace = True)
print(df.to_string())

#Replace Using Mode
x = df["Calories"].mode()[0]
df["Calories"].fillna(x, inplace=True)
print(df.to_string())