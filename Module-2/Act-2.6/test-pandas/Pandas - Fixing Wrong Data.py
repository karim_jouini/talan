#Convert Into a Correct Format
import pandas as pd
df = pd.read_csv("https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
print(df.to_string())

#Replacing Values
df.loc[7, 'Duration'] = 45

for x in df.index:
  if df.loc[x, "Duration"] > 120:
    df.loc[x, "Duration"] = 120
print(df.to_string())

#Removing Rows
df = pd.read_csv("https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
for x in df.index:
  if df.loc[x, "Duration"] > 120:
    df.drop(x, inplace = True)

print(df.to_string())
