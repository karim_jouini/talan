#Discovering Duplicates
import pandas as pd
df = pd.read_csv("https://www.w3schools.com/python/pandas/dirtydata.csv.txt")
print(df.duplicated())

#Removing Duplicates
df.drop_duplicates(inplace = True)
print(df.to_string())
