# import pandas
import pandas

mydataset = {
    'cars': ["BMW", "Volvo", "Ford"],
    'passings': [3, 7, 2]
}

# import pandas
myvar = pandas.DataFrame(mydataset)

print(myvar)

#Checking Pandas Version
import pandas as pd

print(pd.__version__)
