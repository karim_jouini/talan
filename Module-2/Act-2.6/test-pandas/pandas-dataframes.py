#What is a DataFrame?
import pandas as pd

data = {
  "calories": [420, 380, 390],
  "duration": [50, 40, 45]
}

df = pd.DataFrame(data)

print(df)


#Locate Row
print(df.loc[0])

print(df.loc[[0, 2]])


#Named Indexes


data = {
  "calories": [420, 380, 390],
  "duration": [50, 40, 45]
}

df = pd.DataFrame(data, index = ["day1", "day2", "day3"])

print(df)

#Locate Named Indexes
print(df.loc["day2"])








