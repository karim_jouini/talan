for x in df.index:
    if len(str(df.loc[x, "REPLACING-RENAULT-REF"])) != 10:
        df.drop(x, inplace=True)

for x in df.index:

    if str(df.loc[x, "REPLACING-SUPPLIER-NAME"]) in ["aws", "AWS", "Aws"]:
        df.drop(x, inplace=True)

for x in df.index:
    if df.loc[x, "REPLACED-SUPPLIER-REF"] == "CONSOMMABLES":
        df.loc[x, "SHIPPING_DATE"] = "2022"
    elif df.loc[x, "REPLACED-SUPPLIER-REF"] == "NOUVEAU":
        df.loc[x, "SHIPPING_DATE"] = "2023"
    else:
        df.loc[x, "SHIPPING_DATE"] = "2024"

df['REPLACEMENT-DATE'] = pd.to_datetime(df['REPLACEMENT-DATE'], errors='coerce' )

df.dropna(subset=['REPLACEMENT-DATE'], inplace=True)

df.drop_duplicates(subset=['REPLACING-RENAULT-REF'], inplace=True)
df=df.fillna("EMPTY")
print(df.to_string())
print(df.info())

df.to_csv('chain_replacement_clean.csv', sep = '\t')