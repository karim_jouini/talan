class livre():

    def __init__(self, titre, auteur, prix, pages, genre=""):
        self.titre=titre
        self.auteur=auteur
        self.prix=prix
        self.pages=pages
        self.genre=genre
    def afficher(self, titre, auteur, prix, pages, genre=""):
        return titre, auteur, prix, pages, genre
l1=livre("Le petit prince","StExupéry",10.40, 50)
print(l1.afficher(l1.titre,l1.auteur, l1.prix, l1.pages))

l2=livre("Contes","Grimm",14.40,254)
print(l2.afficher(l2.titre,l2.auteur, l2.prix, l2.pages))




class BD(livre):

    def __init__(self,titre, auteur, prix, pages, genre="", taille='small',lecture='GàD', couleur=True):
        livre.__init__(self, titre, auteur, prix, pages, genre)
        self.taille=taille
        self.couleur = str(couleur)
        self.lecture = lecture
    def afficher1 (self,titre, auteur, prix, pages, genre="", taille='small',lecture='GàD', couleur=True):
        return titre, auteur, prix, pages, couleur

b1=BD("Lucky Luke","Morris",10.40, 45, False)
print(b1.afficher(b1.titre, b1.auteur, b1.prix, b1.pages, b1.couleur))

b2=BD("Tintin","Herge",200.40, 45, False)
print(b2.afficher(b2.titre, b2.auteur, b2.prix, b2.pages, b2.couleur))




class MG(livre):
    def __init__(self, titre, auteur, prix, pages, genre="", taille='big', lecture='DàG', couleur=True):
        livre.__init__(self, titre, auteur, prix, pages, genre)
        self.taille = taille
        self.couleur = str(couleur)
        self.lecture = lecture

    def afficher1(self, titre, auteur, prix, pages, genre="", taille='big', lecture='GàD', couleur=True):
        return titre, auteur, prix, pages


m1=BD("One piece","Eiichirō Oda",5.40, 62)
print(m1.afficher(m1.titre, m1.auteur, m1.prix, m1.pages))

m2=BD("One piece","Eiichirō Oda",5.40, 62)
print(m2.afficher(m2.titre, m2.auteur, m2.prix, m2.pages))

class ROMAN(livre):
    lecture= 'DàG'
    def __init__(self,titre, auteur, prix, pages, genre="",nbrchapitre=0,resume=""):
        livre.__init__(self, titre, auteur, prix, pages, genre)
        self.nbrchapitre = nbrchapitre
        self.resume = resume
    def afficher(self, titre, auteur, prix, pages, nbrchapitre=12, resume=""):
        return titre, auteur, pages, prix, nbrchapitre, resume

r1 = ROMAN("Dora","Dora", 300, 3.5)
print(r1.afficher(r1.titre, r1.auteur, r1.prix, r1.pages,12 ,"Une description quelconque"))



class livreRecette(livre):

    def __init__(self,titre, auteur, prix, pages):
        livre.__init__(self, titre, auteur, prix, pages)

    def afficherRecettes(self,titre, auteur, prix, pages):
        return titre, auteur, prix, pages

lrc1 = livreRecette("Marmiton", "Philippe Etchebest", 15.98, 110)
print(lrc1.afficherRecettes(lrc1.titre, lrc1.auteur, lrc1.prix, lrc1.pages))

class Recette():

    def __init__(self,nom, description,niveau=0, astuce="", etapes=""):
        self.nom=nom
        self.description=description
        self.astuce=astuce
        self.niveau=niveau
        self.etapes=etapes
    def afficherRecettes(self,nom, description,niveau=0, astuce="", etapes=""):
        return nom, description, niveau, astuce, etapes

rc1 = Recette("Les pâtes crues", "Comment réaliser de délicieuses pâtes crues", 3, "Ne pas les faire cuire", "Sortir les pâtes de leur emballage")
print(rc1.afficherRecettes(rc1.nom, rc1.description, rc1.niveau, rc1.astuce, rc1.etapes))


