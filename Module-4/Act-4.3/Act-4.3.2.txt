Activité 4.3.2 : Questions

Répondre aux questions suivantes:

1- Donner les Caractéristiques d'un entrepôt de données

    -Orienté sujet: Data warehouse, les données sont organisées par thème.
    -Intégré: Les données proviennent de sources hétérogènes utilisant chacune un type de format.
    -Non volatile:  Les données ne disparaissent pas et ne changent pas au fil des traitements, au fil du temps (Read-Only).
    -Historisé: Les données non volatiles sont aussi horodatées. On peut ainsi visualiser l'évolution dans le temps d'une valeur donnée.

2- Qu'est-ce que INFORMATICA PowerCenter ?
Informatica PowerCenter est un outil ETL utilisé pour extraire, transformer et charger les données d'une entreprise à partir de différentes sources.

3- Quels sont les différents composants de PowerCenter?

Services :
    Repository Service – Responsable de la maintenance des métadonnées Informatica et de la fourniture de l'accès à celles-ci à d'autres services.
    Integration Service – Responsable du mouvement des données des sources vers les cibles
    Reporting Service – Permet la génération de rapports
Calculs :
    Nodes – Plate-forme informatique où les services ci-dessus sont exécutés



4-  Quels sont les différents clients de PowerCenter?

    Informatica Designer – Utilisé pour la création de mappages entre la source et la cible
    Workflow Manager – Utilisé pour créer des flux de travail et d'autres tâches et leur exécution
    Workflow Monitor – Utilisé pour surveiller l'exécution des workflows
    Repository Manager – Utilisé pour gérer les objets dans le référentiel

5- Expliquez PowerCenter Repository.

Utilisez le gestionnaire de référentiels pour administrer les référentiels. Vous pouvez parcourir plusieurs dossiers et référentiels et effectuer les tâches suivantes :

-Gérer les utilisateurs et les groupes. Créez, modifiez et supprimez des utilisateurs et des groupes d'utilisateurs du référentiel. Vous pouvez attribuer et révoquer des privilèges de référentiel
et des autorisations de dossier. 
-Exécuter des fonctions de dossier.
-Afficher les métadonnées. Analysez les sources, les cibles, les mappages et les dépendances de raccourcis, recherchez par mot-clé et affichez les propriétés de PowerCenter

6- Quels sont les types de métadonnées pouvant être stockées dans le Repository 

-Source definitions. 
-Target definitions. 
-Multi-dimensional metadata.
-Mappings.
-Reusable transformations. 
-Mapplets. 
-Sessions and workflows.







