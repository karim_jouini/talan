Activité 5.2.1 : QCM

Soumettre les réponses correctes dans un fichier #activite.txt

1- Power BI Desktop est une application gratuite qui permet à créer des visualisations sur les données exploitées : 

	a- Vrai 

2- Power BI Desktop est une application gratuite qui permet à partager nos rapports à d’autres utilisateurs : 

	b- Faux 

3- En ouvrant Power BI Desktop, La Vue de modèle vous permet d’afficher toutes les données disponibles dans votre rapport :  

	a- Vrai 

4- Un rapport Power BI peut contenir plusieurs pages : 

	a- Vrai 

5- Dans l’interface Power BI Desktop, le volet Champs permet de faire glisser des éléments de requête vers la vue Rapport pour faire interpréter les données : 

	a- Vrai 
